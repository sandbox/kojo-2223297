<?php

/**
 * @file
 * Definition of the 'Tabs to Accordion' panel style.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Responsive Tabs to Accordion'),
  'description' => t('Provides a "tabs" Panel style that switches to accordion on small devices.'),
  'render region' => 'panels_responsive_tabs_to_accordion_style_render_region',
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_panels_responsive_tabs_to_accordion_style_render_region($vars) {
  $display = $vars['display'];
  $region_id = $vars['region_id'];
  $owner_id = $vars['owner_id'];
  $panes = $vars['panes'];

  $tab_id = 'tabs-'. $owner_id . '-' . $region_id;

  $element = array(
    '#prefix' => '<div id="' . $tab_id . '">',
    '#suffix' => '</div>',
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'panels_responsive_tabs_to_accordion') . '/plugins/styles/panels_responsive_tabs_to_accordion.min.js' => array('type' => 'file'),
      ),
      'css' => array(
        drupal_get_path('module', 'panels_responsive_tabs_to_accordion') . '/plugins/styles/panels_responsive_tabs_to_accordion.css' => array('type' => 'file'),
      ),
    ),
  );
  //Declare wrapper id variable to pass it to panels_responsive_tabs_to_accordion.min.js
  drupal_add_js(array('panels_responsive_tabs_to_accordion' => array('tabstoaccordion_panel' => '#'.$tab_id)), array('type' => 'setting'));
  
  $settings = array();
  $settings['panelsTabs']['tabsID'][] = $tab_id;

  $element['#attached']['js'][] = array(
    'type' => 'setting',
    'data' => $settings,
  );

  // Get the pane titles.
  $items = array();
  $delta = 1;

  if (isset($display->panels[$region_id])) {
    foreach ($display->panels[$region_id] as $pane_id) {
      // Make sure the pane exists.
      if (!empty($panes[$pane_id])) {
        $title = panels_responsive_tabs_to_accordion_pane_titles($display->did, $pane_id);
        $title = $title ? $title : t('Tab @delta', array('@delta' => $delta));
        $items[] = '<a href="#' . $tab_id . '-' . $delta . '">' . $title . '</a>';
        $delta++;
      }
      
    }
  }

  if ($delta === 1) {
    // No tabs to show, the tabs wrapper must not be rendered.
    return '';
  }
  $attributes = array(
    'class' => 'resp-tabs-list', 
  );
  $element['tabs_title'] = array('#theme' => 'item_list', '#items' => $items, '#attributes' => $attributes);

  $delta = 1;
  foreach ($panes as $pane_id => $item) {
    $element['tabs_content'][$pane_id] = array(
      '#prefix' => '<div id="' . $tab_id . '-' . $delta . ' " class="resp-tabs-container">',
      '#suffix' => '</div>',
      '#markup' => $item,
    );
    $delta++;
  }

  return drupal_render($element);
}