Responsive Tabs to Accordion Panels
===================================

This project provides a Tabs and Accordion Panel style that can be used by any Panels module.

It relies completely on Panels Tabs code, adapted and mixed with Easy Responsive Tabs to Accordion solution.

    The panel style switches from tabs to accordion depending on a breakpoint that you can setup ( directly in css file).
    You can nest a second level of tabs inside the main one, with same behavior.
    It supports multiple sets of panels tabs in the same page.

